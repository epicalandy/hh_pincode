function pinCodeGenerator(str) {
  let keyPanel = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [undefined, 0, undefined]],
    coordinate = {
      1: [0, 0],
      2: [0, 1],
      3: [0, 2],
      4: [1, 0],
      5: [1, 1],
      6: [1, 2],
      7: [2, 0],
      8: [2, 1],
      9: [2, 2],
      0: [3, 1]
    },
    shift = {up: [0, 1], down: [0, -1], right: [1, 0], left: [-1, 0]},
    userString,
    numVariants = [];

  userString = str.split('');

  function getAllNumsVariants() {
    userString.forEach(function (elm) {
      let num,
        variants = [];

      variants.push(elm);

      Object.keys(shift).forEach(function (key) {
        num = getNextNumVariants(elm, key);
        if (num !== '') variants.push(num);
      });

      numVariants.push(variants);
    });


    function getNextNumVariants(num, act) {
      let x,
        y;

      x = coordinate[num][0] + shift[act][0];
      y = coordinate[num][1] + shift[act][1];

      if (keyPanel[x] && keyPanel[x][y] !== undefined) {
        return '' + keyPanel[x][y];
      }

      return '';
    }

    return numVariants;
  }

  function getCodes(current, next) {
    let result = [];

    current.forEach(function (cur) {
      next.forEach(function (nx) {
        result.push( '' + nx + cur);
      })
    });

    return result;
  }

  function getAllPinCodes(total, next) {
    let newNext;

    if (!next) {
      return total.sort();
    }

    if(!total) {
      total = next.pop();
    }

    newNext = next.pop();

    if (!newNext) {
      return total.sort();
    }

    return getAllPinCodes(getCodes(total, newNext), next).sort();
  }

  getAllNumsVariants();
  return getAllPinCodes(null, numVariants).toString();
}

console.log(pinCodeGenerator('11'));
console.log(pinCodeGenerator('8'));
console.log(pinCodeGenerator('46'));
